package com.newsapi.newsapitask.api;

import com.newsapi.newsapitask.models.response.ArticleResponse;
import com.newsapi.newsapitask.models.response.SourceResponse;

import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by i7-4770k on 29.01.2018
 * API: BaseUrl -> https://newsapi.org/v2/
 * Your API key is: 0003f1c3300148399cabf12edda039de
 */

@Singleton
public interface NewsApi {

    public static final String API_KEY = "0003f1c3300148399cabf12edda039de";

    @GET("sources")
    Call<SourceResponse> getCategory(@Query("apiKey") String apiKey);

    @GET("{where}")
    Call<ArticleResponse> getArticles(
            @Path("where") String where, // everything
            @Query("apiKey") String apiKey,
            @Query("sources") String sources,
            @Query("sortBy") String sortBy, // publishedAt, popularity, relevancy
            @Query("from") String from, // 2018-01-25
            @Query("to") String to, // 2018-01-25
            @Query("page") String page,
            @Query("q") String keywords
    );

    @GET("{where}")
    Call<ArticleResponse> getArticlesTop(
            @Path("where") String where, // top-headlines
            @Query("apiKey") String apiKey,
            @Query("country") String country, // us
            @Query("category") String category,
            @Query("page") String page,
            @Query("q") String keywords
    );

}
