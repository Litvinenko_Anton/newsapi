package com.newsapi.newsapitask.ui.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import com.newsapi.newsapitask.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by CherryPie on 24.11.2017
 */

public class DialogFragmentDatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public void setSelectedListener(OnDateSelected listener) {
        this.listener = listener;
    }

    private OnDateSelected listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        Dialog picker = new DatePickerDialog(getActivity(), this, year, month, day);
        picker.setTitle(getResources().getString(R.string.choose_date));

        return picker;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDateSet(android.widget.DatePicker datePicker, int year, int indexArrayMonth, int day) {
        String dateString = year + "-" + (indexArrayMonth + 1) + "-" + day;

        Date selectedDate = getDate(dateString);

        if (listener != null) {
            listener.onSelectedDate(selectedDate);
        }

    }

    private Date getDate(String dateString) {
        Date selectedDate = new Date();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            selectedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return selectedDate;
    }

    /**
     * Interface
     */
    public interface OnDateSelected {
        void onSelectedDate(Date date);
    }
}
