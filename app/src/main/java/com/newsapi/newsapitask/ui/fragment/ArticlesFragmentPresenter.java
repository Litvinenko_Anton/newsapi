package com.newsapi.newsapitask.ui.fragment;


import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.google.gson.Gson;
import com.newsapi.newsapitask.App;
import com.newsapi.newsapitask.api.NewsApi;
import com.newsapi.newsapitask.models.InputDataModel;
import com.newsapi.newsapitask.models.response.Article;
import com.newsapi.newsapitask.models.response.ArticleResponse;
import com.newsapi.newsapitask.models.response.BaseResponse;
import com.newsapi.newsapitask.ui.adapter.ArticleRealmAdapter;
import com.newsapi.newsapitask.ui.adapter.ArticlesRecyclerAdapter;
import com.utils.myutils.ShareContentUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.utils.myutils.DebugMessageUtils.logD;
import static com.utils.myutils.DebugMessageUtils.logE;


/**
 * Presenter
 */

@InjectViewState
public class ArticlesFragmentPresenter extends MvpPresenter<ArticlesFragmentPresenter.ArticlesFragmentPresenterView> implements ArticleRealmAdapter.OnResultsChangeListener {

    @Inject
    NewsApi api;
    @Inject
    ArticleRealmAdapter realmAdapter;
    private RealmResults<Article> realmResults;


    private InputDataModel inputData;
    private ArticlesRecyclerAdapter recyclerAdapter;
    private List<Article> list = new ArrayList<>();
    private int totalResults;

    public void startConfig() {
        App.getAppComponent().inject(this);
        this.inputData = new InputDataModel();
        realmResults = realmAdapter.getRealmResults();
        realmAdapter.setOnResultsChangeListener(this);
        getViewState().initInputDate(inputData);
        initRecyclerView(list);
    }

    private void initRecyclerView(List<Article> list) {
        if (recyclerAdapter == null) {
            recyclerAdapter = new ArticlesRecyclerAdapter(list);
            getViewState().initRecyclerViewAdapter(recyclerAdapter, list);
        }
    }

    public void reloadArticles() {
        if (list.size() > 0)
            list.clear();
        loadMoreArticles();
    }

    public void loadMoreArticles(int page, int totalItemsCount) {
        if (totalItemsCount < totalResults) {
            inputData.setPage(String.valueOf(page));
            loadMoreArticles();
        }
    }

    private void loadMoreArticles() {
        if (inputData.isAllArticles())
            loadAllArticles(inputData);
        else
            loadTopArticles(inputData);
    }

    private void loadTopArticles(InputDataModel inputData) {
        loadArticles(api.getArticlesTop(inputData.getWhere(),
                NewsApi.API_KEY,
                inputData.getCountry(),
                inputData.getCategory(),
                inputData.getPage(),
                inputData.getKeywords()));
    }

    private void loadAllArticles(InputDataModel inputData) {
        loadArticles(api.getArticles(inputData.getWhere(),
                NewsApi.API_KEY,
                inputData.getSources(),
                inputData.getSortBy(),
                inputData.getFrom(),
                inputData.getTo(),
                inputData.getPage(),
                inputData.getKeywords()));
    }

    private void loadArticles(Call<ArticleResponse> call) {
        call.enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                ArticleResponse bodyResponse = response.body();
                if (bodyResponse != null && bodyResponse.isSuccess()) {
                    logD("onSuccess -> " + response.raw().request().url().toString());
                    applyComplete(bodyResponse);
                } else {
                    applyFailure(response);
                }
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                applyFailure(call, t);
            }
        });
    }

    private void applyComplete(ArticleResponse bodyResponse) {
        list.addAll(checkContainsInFavorites(bodyResponse.getArticles()));
        totalResults = bodyResponse.getTotalResults();
        checkContainsInFavorites(list);
        getViewState().responseComplete(list, bodyResponse.getTotalResults());
    }

    private List<Article> checkContainsInFavorites(List<Article> list) {
        for (Article article : list) {
            article.setFavorites(realmAdapter.containsInRealmFinds(article));
        }
        return list;
    }

    private void applyFailure(Response response) {
        logE("onFailure ->" + response.raw().request().url().toString());
        BaseResponse baseResponse = null;
        try {
            baseResponse = new Gson().fromJson(response.errorBody().string(), BaseResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
            getViewState().responseError("Request is failure");
            return;
        }
        logD(baseResponse.getMessage());
        getViewState().responseError(baseResponse.getMessage());
    }


    private void applyFailure(Call call, Throwable t) {
        logE("onFailure ->" + call.request().url().toString());
        if (t != null)
            t.printStackTrace();
        getViewState().responseError("Request is failure");
    }

    public void onFavoritesClicked(int positionClick) {
        Article article = recyclerAdapter.getList().get(positionClick);
        if (article.isFavorites()) {
            article.setFavorites(false);
            realmAdapter.deleteFromRealm(article);
        } else {
            article.setFavorites(true);
            realmAdapter.saveToRealm(article);
        }
    }

    @Override
    public void resultsOnChange(RealmResults<Article> articles) {
        checkContainsInFavorites(list);
        getViewState().notifyRecyclerView();
    }

    public void onSharedClicked(Context context, int positionClick) {
        Article article = list.get(positionClick);
        ShareContentUtils.sharedContent(context, "Share", article.getTitle()
                .concat("\n").concat(article.getUrl()));
    }

    /**
     * ViewState
     */

    @StateStrategyType(AddToEndSingleStrategy.class)
    public interface ArticlesFragmentPresenterView extends MvpView {

        void initInputDate(InputDataModel inputDataModel);

        void responseComplete(List<Article> list, int totalResults);

        void responseError(String errorMessage);

        void notifyRecyclerView();

        void initRecyclerViewAdapter(ArticlesRecyclerAdapter adapter, List<Article> list);

    }
}
