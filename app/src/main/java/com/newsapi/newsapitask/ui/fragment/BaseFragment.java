package com.newsapi.newsapitask.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.utils.myutils.ViewUtils.setVisibleOrGoneView;


/**
 * Created by Anton on 23.05.2017
 */

public class BaseFragment extends MvpAppCompatFragment {

    protected Unbinder unbinder;

    protected void bindButterKnife(@NonNull Fragment fragment, View view) {
        unbinder = ButterKnife.bind(fragment, view);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroy();
    }

    public void stopOurSelf() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    protected void setMessageVisibility(TextView textView, String message, int responseCount) {
        if (setVisibleOrGoneView(textView, responseCount == 0))
            textView.setText(message);
    }

    protected void setProgressBarVisibility(ProgressBar progressBarView, boolean isVisible) {
        setVisibleOrGoneView(progressBarView, isVisible);
    }

    protected void notifyRecyclerView(RecyclerView recyclerView) {
        if (recyclerView != null && recyclerView.getAdapter() != null)
            recyclerView.getAdapter().notifyDataSetChanged();
    }

    protected void initRecyclerViewAdapter(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        if (recyclerView != null && adapter != null && recyclerView.getAdapter() != adapter)
            recyclerView.setAdapter(adapter);
    }

}
