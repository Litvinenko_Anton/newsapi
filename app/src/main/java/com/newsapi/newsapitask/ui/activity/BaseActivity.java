package com.newsapi.newsapitask.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.AnimRes;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.newsapi.newsapitask.R;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.utils.myutils.ViewUtils.setVisibleOrGoneView;

/**
 * Created by Anton on 23.05.2017
 */

public abstract class BaseActivity extends MvpAppCompatActivity {

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        unbindButterKnife();
        super.onDestroy();
    }

    protected void bindButterKnife(Activity activity) {
        if (activity != null)
            unbinder = ButterKnife.bind(activity);
    }

    protected void unbindButterKnife() {
        if (unbinder != null)
            unbinder.unbind();
    }

    protected void setMessageVisibility(TextView textView, String message, int responseCount) {
        if (setVisibleOrGoneView(textView, responseCount == 0))
            textView.setText(message);
    }

    protected void setProgressBarVisibility(ProgressBar progressBarView, boolean isVisible) {
        setVisibleOrGoneView(progressBarView, isVisible);
    }

    protected void notifyRecyclerView(RecyclerView recyclerView) {
        if (recyclerView != null && recyclerView.getAdapter() != null)
            recyclerView.getAdapter().notifyDataSetChanged();
    }

    protected boolean hideKeyBoard() {
        if (getCurrentFocus() != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            return true;
        }
        return false;
    }

    public void startFragment(Fragment fragment, @IdRes int containerViewId, String TAG) {
        startFragmentAddStack(
                fragment,
                containerViewId,
                TAG,
                R.anim.enter_from_right,
                R.anim.exit_to_right);
    }

    private void startFragmentAddStack(Fragment fragment, @IdRes int containerViewId, String TAG, @AnimRes int enter, @AnimRes int exit) {
        if (fragment != null && TAG != null && getSupportFragmentManager().findFragmentByTag(TAG) == null) {
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(enter, exit, enter, exit)
                    .add(containerViewId, fragment, TAG)
                    .addToBackStack(TAG)
                    .commit();
        }
    }

}
