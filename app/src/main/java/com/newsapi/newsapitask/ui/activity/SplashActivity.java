package com.newsapi.newsapitask.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.newsapi.newsapitask.R;

import static com.utils.myutils.DebugMessageUtils.logDTV;

public class SplashActivity extends BaseActivity {
    public static final String TAG = "SplashActivity";

    public static Intent getIntent(final Context context) {
        Intent intent = new Intent(context, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logDTV("SplashActivity TEST");
//        chooseTransition();
        nextActivityNoAnim(CategoryActivity.getIntent(SplashActivity.this));
    }

//    private void chooseTransition() {
//        if (checkSingleton())
//            if (sharedPrefManager.getLoginState()) {
//                userData = sharedPrefManager.getUserData();
//                signInRequest(this, userData);
//            } else {
//                nextActivityNoAnim(SignInActivity.getIntent(SplashActivity.this));
//            }
//    }


    private void nextActivityNoAnim(Intent activityIntent) {
        startActivity(activityIntent);
        overridePendingTransition(R.anim.no_anim, R.anim.no_anim);
        finish();
    }

}
