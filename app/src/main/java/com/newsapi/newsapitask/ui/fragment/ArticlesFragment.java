package com.newsapi.newsapitask.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding2.widget.RxAdapterView;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;
import com.jakewharton.rxbinding2.widget.RxRadioGroup;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.newsapi.newsapitask.R;
import com.newsapi.newsapitask.models.Category;
import com.newsapi.newsapitask.models.InputDataModel;
import com.newsapi.newsapitask.models.response.Article;
import com.newsapi.newsapitask.models.response.Source;
import com.newsapi.newsapitask.tools.EndlessRecyclerViewScrollListener;
import com.newsapi.newsapitask.tools.ResUtils;
import com.newsapi.newsapitask.ui.adapter.ArticlesRecyclerAdapter;
import com.nikhilpanju.recyclerviewenhanced.RecyclerTouchListener;
import com.utils.myutils.ConvertDateUtil;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static com.newsapi.newsapitask.App.toast;
import static com.utils.myutils.DebugMessageUtils.logD;


public class ArticlesFragment extends BaseFragment implements ArticlesFragmentPresenter.ArticlesFragmentPresenterView {

    public static final String TAG = "ArticlesFragment";
    public static final String POST = "POST";
    private EndlessRecyclerViewScrollListener scrollListener;
    private InputDataModel inputDataModel;
    private Category category;
    private List<Article> articleList;

//    private ItemSelectedListener mItemSelectedListener;

    @InjectPresenter
    ArticlesFragmentPresenter mArticlesFragmentPresenter;

    @BindView(R.id.whereSwitch)
    Switch whereSwitch;
    @BindView(R.id.sortRadioGroup)
    RadioGroup sortRadioGroup;
    @BindView(R.id.publishedRadioButton)
    RadioButton publishedRadioButton;
    @BindView(R.id.popularityRadioButton)
    RadioButton popularityRadioButton;
    @BindView(R.id.relevancyRadioButton)
    RadioButton relevancyRadioButton;
    @BindView(R.id.fromTextView)
    TextView fromTextView;
    @BindView(R.id.toTextView)
    TextView toTextView;
    @BindView(R.id.keyWords)
    EditText keyWords;
    @BindView(R.id.totalResultsCountsTextView)
    TextView totalResultsCountsTextView;

    @BindView(R.id.messageTextView)
    TextView messageTextView;
    @BindView(R.id.domainsSpinnerView)
    Spinner domainsSpinnerView;
    @BindView(R.id.progressBarView)
    ProgressBar progressBarView;


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private int totalResults;

    public static ArticlesFragment newInstance(Category category) {
        ArticlesFragment fragment = new ArticlesFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(POST, category);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        mItemSelectedListener = (ItemSelectedListener) getActivity();
    }

    @Override
    public void onDetach() {
//        mItemSelectedListener = null;
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.articles_content, container, false);
        bindButterKnife(this, mView);
        mArticlesFragmentPresenter.startConfig();
        initArguments();
        initView();
        return mView;
    }

    private void initArguments() {
        if (getArguments() != null)
            category = (Category) getArguments().getSerializable(POST);
    }

    private void initView() {
        initSwitchView(whereSwitch);
        initRadioGroupView(sortRadioGroup);
        initDateView(fromTextView);
        initDateView(toTextView);
        initSpinnerView(domainsSpinnerView, category);
        initRxTextView(keyWords);
        initRecyclerView();
    }

    private void initSwitchView(Switch whereSwitch) {
        RxCompoundButton.checkedChanges(whereSwitch)
                .skipInitialValue()
                .subscribe(stateChanged -> {
                    inputDataModel.setWhere((stateChanged) ? "everything" : "top-headlines");
                    inputDataModel.skipPage();
                    reloadArticles();
                });
    }

    private void initRadioGroupView(RadioGroup radioGroup) {
        RxRadioGroup.checkedChanges(radioGroup)
                .skipInitialValue()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(viewId -> {
                    logD("RadioGroup " + viewId.toString());
                    switch (viewId) {
                        case R.id.publishedRadioButton:
                            inputDataModel.setSortBy("publishedAd");
                            break;
                        case R.id.popularityRadioButton:
                            inputDataModel.setSortBy("popularity");
                            break;
                        case R.id.relevancyRadioButton:
                            inputDataModel.setSortBy("relevancy");
                            break;
                    }
                    reloadArticles();
                });
    }

    private void initDateView(TextView view) {
        DialogFragmentDatePicker datePickerFragment = new DialogFragmentDatePicker();
        datePickerFragment.setSelectedListener(date -> {
            view.setText(ConvertDateUtil.convertDateToString(date, ConvertDateUtil.YYYY_MM_DD));
            inputDataModel.setFrom(fromTextView.getText().toString());
            inputDataModel.setTo(toTextView.getText().toString());
            reloadArticles();
        });
        view.setOnClickListener(v -> datePickerFragment.show(getActivity().getFragmentManager(), "datePicker"));
    }

    private void initSpinnerView(Spinner spinnerView, Category category) {
        String[] data = prepareSpinnerDateArr(category);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerView.setAdapter(adapter);

        RxAdapterView.itemSelections(spinnerView)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(integer -> {
                    String sources = inputDataModel.getSources();
                    inputDataModel.setCountry(category.getSources().get(integer).getCountry());
                    inputDataModel.setSources(category.getSources().get(integer).getName());
                    if (sources != null && !sources.isEmpty())
                        reloadArticles();
                });
    }

    private String[] prepareSpinnerDateArr(Category category) {
        List<Source> list = category.getSources();
        String[] data = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            data[i] = list.get(i).getName();
        }
        return data;
    }

    private Disposable initRxTextView(EditText editText) {
        return RxTextView.textChangeEvents(editText)
                .skipInitialValue()
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(textEvent -> {
                    inputDataModel.setKeywords(editText.getText().toString());
                    reloadArticles();
                });
    }

    private void reloadArticles() {
        if (inputDataModel.requestIsPossible()) {
            setProgressBarVisibility(progressBarView, true);
            mArticlesFragmentPresenter.reloadArticles();
        }
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(recyclerView.getContext());
        recyclerView.setLayoutManager(layoutManager);
        initTouchListener();
        initScrollListener(layoutManager); //Scroll to loadMore
    }

    private void initTouchListener() {
        RecyclerTouchListener onTouchListener = new RecyclerTouchListener(getActivity(), recyclerView);
        onTouchListener
                .setIndependentViews(R.id.favoritesImageButton, R.id.shareImageButton)
                .setClickable(new RecyclerTouchListener.OnRowClickListener() {
                    @Override
                    public void onRowClicked(int positionClick) {
                        //TODO do something
                    }

                    @Override
                    public void onIndependentViewClicked(int independentViewID, int positionClick) {
                        switch (independentViewID) {
                            case R.id.favoritesImageButton:
                                mArticlesFragmentPresenter.onFavoritesClicked(positionClick);
                                notifyRecyclerView(recyclerView);
                                break;
                            case R.id.shareImageButton:
                                mArticlesFragmentPresenter.onSharedClicked(getContext(), positionClick);
                                //TODO
                                break;
                        }
                    }
                });
        recyclerView.addOnItemTouchListener(onTouchListener);
    }

    private void initScrollListener(LinearLayoutManager layoutManager) {
        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                logD("onLoadMore page = " + page + " | totalItemsCount:" + totalItemsCount);
                if (totalItemsCount < totalResults) {
                    setProgressBarVisibility(progressBarView, true);
                    mArticlesFragmentPresenter.loadMoreArticles(page, totalItemsCount);
                }
            }
        };
        recyclerView.addOnScrollListener(scrollListener);
    }

    @Override
    public void initInputDate(InputDataModel inputDataModel) {
        inputDataModel.setCategory(category.getCategory());
        inputDataModel.setCountry(category.getSources().get(0).getCountry());
        this.inputDataModel = inputDataModel;
        reloadArticles();
    }

    @Override
    public void responseComplete(List<Article> list, int totalResults) {
        this.totalResults = totalResults;
        logD("totalResults " + totalResults);
        totalResultsCountsTextView.setText(getString(R.string.total_results_counts, totalResults));
        setMessageVisibility(messageTextView, ResUtils.getStringRes(R.string.not_found), list.size());
        setProgressBarVisibility(progressBarView, false);
        notifyRecyclerView();
    }

    @Override
    public void notifyRecyclerView() {
        notifyRecyclerView(recyclerView);
    }

    @Override
    public void responseError(String errorMessage) {
        toast(errorMessage);
        setProgressBarVisibility(progressBarView, false);
        notifyRecyclerView(recyclerView);
    }

    @Override
    public void initRecyclerViewAdapter(ArticlesRecyclerAdapter adapter, List<Article> list) {
        this.articleList = list;
        initRecyclerViewAdapter(recyclerView, adapter);
    }
}
