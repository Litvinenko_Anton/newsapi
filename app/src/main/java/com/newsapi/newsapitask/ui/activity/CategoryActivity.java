package com.newsapi.newsapitask.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.newsapi.newsapitask.R;
import com.newsapi.newsapitask.models.Category;
import com.newsapi.newsapitask.models.response.Source;
import com.newsapi.newsapitask.tools.ResUtils;
import com.newsapi.newsapitask.ui.adapter.CategoryRecyclerAdapter;
import com.newsapi.newsapitask.ui.fragment.ArticlesFragment;
import com.newsapi.newsapitask.ui.fragment.FavoritesFragment;
import com.nikhilpanju.recyclerviewenhanced.RecyclerTouchListener;
import com.utils.myutils.DebugMessageUtils;

import java.util.List;

import butterknife.BindView;

import static com.newsapi.newsapitask.App.toast;

public class CategoryActivity extends BaseActivity implements
        CategoryPresenter.CategoryPresenterView {

    @InjectPresenter
    CategoryPresenter mCategoryPresenter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.messageTextView)
    TextView messageTextView;
    @BindView(R.id.progressBarView)
    ProgressBar progressBarView;

    private List<Category> categoryList;

    public static Intent getIntent(final Context context) {
        return new Intent(context, CategoryActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindButterKnife(this);

        initRecyclerView();

        setProgressBarVisibility(progressBarView, true);
        mCategoryPresenter.startConfig();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DebugMessageUtils.where(this)
                .setVisibility(false)
                .setOnOffLongClickableView(toolbar)
                .setClearLongClickableView(DebugMessageUtils.getLogTextView())
                .setTextSize(8)
                .setMaxLines(15)
                .setMaxLogLength(300)
                .build();
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(recyclerView.getContext());
        recyclerView.setLayoutManager(layoutManager);
        initTouchListener();
    }

    private void initTouchListener() {
        RecyclerTouchListener onTouchListener = new RecyclerTouchListener(this, recyclerView);
        onTouchListener
                .setClickable(new RecyclerTouchListener.OnRowClickListener() {
                    @Override
                    public void onRowClicked(int positionClick) {
                        Category category = categoryList.get(positionClick);
                        startFragment(ArticlesFragment.newInstance(category),
                                R.id.fragmentContainer,
                                ArticlesFragment.TAG);
                    }

                    @Override
                    public void onIndependentViewClicked(int independentViewID, int positionClick) {

                    }
                });
        recyclerView.addOnItemTouchListener(onTouchListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.favorites:
                startFragment(FavoritesFragment.newInstance(),
                        R.id.fragmentContainer,
                        FavoritesFragment.TAG);
                break;
            case R.id.settings:
                toast("TODO");                                      //TODO
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void responseComplete(List<Category> categoryList, List<Source> sources) {
        setMessageVisibility(messageTextView, ResUtils.getStringRes(R.string.not_found), categoryList.size());
        setProgressBarVisibility(progressBarView, false);
        notifyRecyclerView(recyclerView);
    }

    @Override
    public void responseError(String errorMessage) {
        toast(errorMessage);
        setProgressBarVisibility(progressBarView, false);
        notifyRecyclerView(recyclerView);
    }

    @Override
    public void initRecyclerViewAdapter(CategoryRecyclerAdapter adapter, List<Category> categoryList) {
        this.categoryList = categoryList;
        if (recyclerView.getAdapter() != adapter)
            recyclerView.setAdapter(adapter);
    }
}
