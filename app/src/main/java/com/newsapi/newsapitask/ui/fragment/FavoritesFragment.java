
package com.newsapi.newsapitask.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.newsapi.newsapitask.R;
import com.newsapi.newsapitask.models.response.Article;
import com.newsapi.newsapitask.tools.ResUtils;
import com.newsapi.newsapitask.ui.adapter.FavoritesRecyclerAdapter;
import com.nikhilpanju.recyclerviewenhanced.RecyclerTouchListener;

import butterknife.BindView;
import io.realm.RealmResults;


public class FavoritesFragment extends BaseFragment implements FavoritesFragmentPresenter.FavoritesFragmentPresenterView {

    public static final String TAG = "FavoritesFragment";

    @InjectPresenter
    FavoritesFragmentPresenter mFavoritesFragmentPresenter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.messageTextView)
    TextView messageTextView;

    public static FavoritesFragment newInstance() {
        return new FavoritesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.favorites_content, container, false);
        bindButterKnife(this, mView);
        initRecyclerView();
        mFavoritesFragmentPresenter.startConfig();
        return mView;
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(recyclerView.getContext());
        recyclerView.setLayoutManager(layoutManager);
        initTouchListener();
    }

    private void initTouchListener() {
        RecyclerTouchListener onTouchListener = new RecyclerTouchListener(getActivity(), recyclerView);
        onTouchListener
                .setIndependentViews(R.id.favoritesImageButton, R.id.shareImageButton)
                .setClickable(new RecyclerTouchListener.OnRowClickListener() {
                    @Override
                    public void onRowClicked(int positionClick) {
                        //TODO do something
                    }

                    @Override
                    public void onIndependentViewClicked(int independentViewID, int positionClick) {
                        switch (independentViewID) {
                            case R.id.favoritesImageButton:
                                mFavoritesFragmentPresenter.onFavoritesClicked(positionClick);
                                break;
                            case R.id.shareImageButton:
                                mFavoritesFragmentPresenter.onSharedClicked(getContext(), positionClick);
                                break;
                        }
                    }
                });
        recyclerView.addOnItemTouchListener(onTouchListener);
    }

    @Override
    public void notifyRecyclerView(RealmResults<Article>  list) {
        setMessageVisibility(messageTextView, ResUtils.getStringRes(R.string.not_found), list.size());
        notifyRecyclerView(recyclerView);
    }

    @Override
    public void initRecyclerViewAdapter(FavoritesRecyclerAdapter adapter, RealmResults<Article> realmResults) {
        initRecyclerViewAdapter(recyclerView, adapter);
    }
}
