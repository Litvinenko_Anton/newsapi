package com.newsapi.newsapitask.ui.fragment;


import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.newsapi.newsapitask.App;
import com.newsapi.newsapitask.models.response.Article;
import com.newsapi.newsapitask.ui.adapter.ArticleRealmAdapter;
import com.newsapi.newsapitask.ui.adapter.FavoritesRecyclerAdapter;
import com.utils.myutils.ShareContentUtils;

import javax.inject.Inject;

import io.realm.RealmResults;


/**
 * Presenter
 */

@InjectViewState
public class FavoritesFragmentPresenter extends MvpPresenter<FavoritesFragmentPresenter.FavoritesFragmentPresenterView> implements ArticleRealmAdapter.OnResultsChangeListener {

    @Inject
    ArticleRealmAdapter realmAdapter;
    private RealmResults<Article> realmResults;
    private FavoritesRecyclerAdapter recyclerAdapter;


    public void startConfig() {
        App.getAppComponent().inject(this);
        realmAdapter.setOnResultsChangeListener(this);
        initRecyclerView(realmAdapter.getRealmResults());
    }

    private void initRecyclerView(RealmResults<Article> realmResults) {
        if (recyclerAdapter == null) {
            this.realmResults = realmResults;
            recyclerAdapter = new FavoritesRecyclerAdapter(realmAdapter.getRealmResults());
            getViewState().initRecyclerViewAdapter(recyclerAdapter, realmResults);
            getViewState().notifyRecyclerView(realmResults);
        }
    }

    public void onFavoritesClicked(int positionClick) {
        Article article = recyclerAdapter.getList().get(positionClick);
        realmAdapter.deleteFromRealm(article);
    }

    public void onSharedClicked(Context context, int positionClick) {
        Article article = recyclerAdapter.getList().get(positionClick);
        ShareContentUtils.sharedContent(context, "Share", article.getTitle()
                .concat("\n").concat(article.getUrl()));
    }

    @Override
    public void resultsOnChange(RealmResults<Article> articles) {
        getViewState().notifyRecyclerView(articles);
    }

    /**
     * ViewState
     */

    @StateStrategyType(AddToEndSingleStrategy.class)
    public interface FavoritesFragmentPresenterView extends MvpView {

        void notifyRecyclerView(RealmResults<Article> list);

        void initRecyclerViewAdapter(FavoritesRecyclerAdapter adapter, RealmResults<Article> realmResults);
    }
}
