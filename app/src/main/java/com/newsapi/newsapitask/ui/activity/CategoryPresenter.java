package com.newsapi.newsapitask.ui.activity;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.google.gson.Gson;
import com.newsapi.newsapitask.App;
import com.newsapi.newsapitask.api.NewsApi;
import com.newsapi.newsapitask.models.Category;
import com.newsapi.newsapitask.models.response.BaseResponse;
import com.newsapi.newsapitask.models.response.Source;
import com.newsapi.newsapitask.models.response.SourceResponse;
import com.newsapi.newsapitask.ui.adapter.CategoryRecyclerAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.utils.myutils.DebugMessageUtils.logD;
import static com.utils.myutils.DebugMessageUtils.logE;


/**
 * Presenter
 */

@InjectViewState
public class CategoryPresenter extends MvpPresenter<CategoryPresenter.CategoryPresenterView> {

    @Inject
    NewsApi api;

    private CategoryRecyclerAdapter adapter;
    private List<Category> list = new ArrayList<>();

    public void startConfig() {
        App.getAppComponent().inject(this);
        initRecyclerView(list);
        loadCategory();
    }

    private void initRecyclerView(List<Category> list) {
        if (adapter == null) {
            adapter = new CategoryRecyclerAdapter(list);
            getViewState().initRecyclerViewAdapter(adapter, list);
        }
    }


    public void loadCategory() {
        api.getCategory(NewsApi.API_KEY).enqueue(new Callback<SourceResponse>() {
            @Override
            public void onResponse(Call<SourceResponse> call, Response<SourceResponse> response) {
                SourceResponse bodyResponse = response.body();
                if (bodyResponse != null && bodyResponse.isSuccess()) {
                    logD("onSuccess -> " + response.raw().request().url().toString());
                    applyComplete(bodyResponse);
                } else {
                    applyFailure(response);
                }
            }

            @Override
            public void onFailure(Call<SourceResponse> call, Throwable t) {
                applyFailure(call, t);
            }
        });
    }

    private void applyComplete(SourceResponse bodyResponse) {
        List<Category> responseList = bodyResponse.getCategoryList();
        getViewState().responseComplete(fillCategoryList(responseList), bodyResponse.getSources());
    }

    private List<Category> fillCategoryList(List<Category> responseList) {
        if (list.size() > 0)
            list.clear();
        list.addAll(responseList);
        return list;
    }

    private void applyFailure(Response response) {
        logE("onFailure ->" + response.raw().request().url().toString());
        BaseResponse baseResponse = null;
        try {
            baseResponse = new Gson().fromJson(response.errorBody().string(), BaseResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
            getViewState().responseError("Request is failure");
            return;
        }
        logD(baseResponse.getMessage());
        getViewState().responseError(baseResponse.getMessage());
    }

    private void applyFailure(Call call, Throwable t) {
        logE("onFailure ->" + call.request().url().toString());
        if (t != null)
            t.printStackTrace();
        getViewState().responseError("Request is failure");
    }

    /**
     * ViewState
     */
    @StateStrategyType(AddToEndSingleStrategy.class)
    public interface CategoryPresenterView extends MvpView {

        void responseComplete(List<Category> categoryList, List<Source> sources);

        void responseError(String errorMessage);

        void initRecyclerViewAdapter(CategoryRecyclerAdapter adapter, List<Category> categoryList);
    }
}
