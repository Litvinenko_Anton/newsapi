package com.newsapi.newsapitask.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.newsapi.newsapitask.R;
import com.newsapi.newsapitask.models.Category;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Anton on 29.01.2018
 */

public class CategoryRecyclerAdapter extends RecyclerView.Adapter<CategoryRecyclerAdapter.ViewHolder> {

    private List<Category> itemList = new ArrayList<>();

    public CategoryRecyclerAdapter(List<Category> list) {
        itemList = list;
    }

    public void setList(List<Category> list) {
        itemList = list;
        notifyDataSetChanged();
    }

    public List<Category> getList() {
        return itemList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_category_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Category item = itemList.get(position);

        holder.titleTextView.setText(holder.titleTextView.getContext().
                getString(R.string.category,
                        item.getCategory(),
                        item.getSources().size()));
    }

    @Override
    public int getItemCount() {
        return (itemList == null) ? 0 : itemList.size();
    }


    /**
     * ---------- ViewHolder ----------
     */
    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.titleTextView)
        TextView titleTextView;

        private ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
