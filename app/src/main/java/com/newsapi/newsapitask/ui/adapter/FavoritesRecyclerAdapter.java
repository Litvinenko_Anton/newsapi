package com.newsapi.newsapitask.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.newsapi.newsapitask.R;
import com.newsapi.newsapitask.models.response.Article;
import com.utils.myutils.ViewUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;

/**
 * Created by Anton on 29.01.2018
 */

public class FavoritesRecyclerAdapter extends RecyclerView.Adapter<FavoritesRecyclerAdapter.ViewHolder> {

    private RealmResults<Article> itemList;

    public FavoritesRecyclerAdapter(RealmResults<Article> list) {
        itemList = list;
    }

    public void setList(RealmResults<Article> list) {
        itemList = list;
        notifyDataSetChanged();
    }

    public RealmResults<Article> getList() {
        return itemList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_articles_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Article item = itemList.get(position);

        holder.titleTextView.setText(item.getTitle());
        holder.descriptionTextView.setText(item.getDescription());
        holder.dateTextView.setText(item.getPublishedAtFormat());

        ViewUtils.setVisibleOrInvisibleView(holder.logoImageView,
                item.getUrlToImage() != null);
        Glide.with(holder.logoImageView.getContext())
                .load(item.getUrlToImage())
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .into(holder.logoImageView);
    }

    @Override
    public int getItemCount() {
        return (itemList == null) ? 0 : itemList.size();
    }


    /**
     * ---------- ViewHolder ----------
     */
    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.logoImageView)
        ImageView logoImageView;
        @BindView(R.id.titleTextView)
        TextView titleTextView;
        @BindView(R.id.descriptionTextView)
        TextView descriptionTextView;
        @BindView(R.id.dateTextView)
        TextView dateTextView;
        @BindView(R.id.favoritesImageButton)
        ImageButton favoritesImageButton;

        private ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
