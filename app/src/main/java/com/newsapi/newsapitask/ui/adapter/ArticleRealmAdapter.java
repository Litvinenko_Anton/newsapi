package com.newsapi.newsapitask.ui.adapter;

import com.newsapi.newsapitask.models.response.Article;
import com.newsapi.newsapitask.tools.RealmStorage;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

import static com.utils.myutils.DebugMessageUtils.logD;

/**
 * Created by i7-4770k on 29.01.2018
 */

public class ArticleRealmAdapter implements RealmStorage<Article>, RealmChangeListener<RealmResults<Article>> {

    private Realm realm;
    private RealmResults<Article> articles;
    private OnResultsChangeListener listener;

    public ArticleRealmAdapter(Realm realm) {
        this.realm = realm;
        findAll();
    }

    private RealmResults<Article> findAll() {
        articles = realm.where(Article.class).findAll();
        articles.addChangeListener(this);
        return articles;
    }

    public RealmResults<Article> getRealmResults() {
        return articles;
    }

    public void setOnResultsChangeListener(OnResultsChangeListener listener) {
        this.listener = listener;
    }

    public void saveAllToRealm(final List<Article> objects) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for (Article article : objects) {
                    realm.copyToRealmOrUpdate(article);
                }
            }
        });
    }

    public void saveToRealm(final Article object) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                int key = object.getPrimaryKey(); // init key
                logD("saveToRealm ID:" + key);
                realm.copyToRealmOrUpdate(object);
            }
        });
    }

    @Override
    public boolean containsInRealm(Article object) {
        Article result = null;
        try {
            realm.beginTransaction();
            result = realm.where(Article.class)
                    .contains("id", String.valueOf(object.getPrimaryKey()))
                    .findFirst();
            realm.commitTransaction();
        } catch (Exception e) {
            realm.cancelTransaction();
        }
        return (result != null);
    }

    @Override
    public boolean containsInRealmFinds(Article object) {
        if (articles == null || articles.size() == 0)
            return false;

        int id = object.getPrimaryKey();
        for (Article item : articles) {
            if (item.getPrimaryKey() == id)
                return true;
        }

        return false;
    }

    @Override
    public Article getRealmObjectById(int id) {
        if (articles == null || articles.size() == 0)
            return null;

        for (Article item : articles) {
            if (item.getPrimaryKey() == id)
                return item;
        }

        return null;
    }

    @Override
    public void deleteFromRealm(Article object) {
        Article article = getRealmObjectById(object.getPrimaryKey());
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                logD("deleteFromRealm ID:" + object.getPrimaryKey());
                if (article != null)
                    article.deleteFromRealm();
            }
        });
    }

    @Override
    public void deleteAllFromRealm(final List<Article> objects) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for (Article object : objects) {
                    object.deleteFromRealm();
                }
            }
        });
    }

    @Override
    public void onChange(RealmResults<Article> articles) {
        if (listener != null)
            listener.resultsOnChange(articles);
    }

    @Override
    public void closeConnection() {
        if (realm != null) {
            realm.close();
            logD("Close on finished thread: " + Thread.currentThread().getName());
        }
    }

    @Override
    public void onDestroy() {
        if (articles != null)
            articles.removeChangeListener(this);
        listener = null;
        articles = null;
    }

    @Override
    public void clearRealm() {
        if (articles != null)
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    articles.deleteAllFromRealm();
                }
            });
    }

    public interface OnResultsChangeListener {
        void resultsOnChange(RealmResults<Article> articles);
    }
}
