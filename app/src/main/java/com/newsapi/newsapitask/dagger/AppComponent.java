package com.newsapi.newsapitask.dagger;

import com.newsapi.newsapitask.ui.activity.CategoryPresenter;
import com.newsapi.newsapitask.ui.fragment.ArticlesFragmentPresenter;
import com.newsapi.newsapitask.ui.fragment.FavoritesFragmentPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by i7-4770k on 29.01.2018
 */

@Singleton
@Component(modules = {AppModule.class, RetrofitModule.class, RealmModule.class})
public interface AppComponent {

    void inject(CategoryPresenter categoryPresenter);

    void inject(ArticlesFragmentPresenter articlesFragmentPresenter);

    void inject(FavoritesFragmentPresenter favoritesFragmentPresenter);

}
