package com.newsapi.newsapitask.dagger;

import android.content.Context;

import com.newsapi.newsapitask.ui.adapter.ArticleRealmAdapter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by i7-4770k on 29.01.2018
 */

@Module
public class RealmModule {

    public RealmModule(Context context) {
        Realm.init(context);
    }

    @Provides
    @Singleton
    Realm provideRealm(RealmConfiguration realmConfiguration) {
        Realm.setDefaultConfiguration(realmConfiguration);
        return Realm.getDefaultInstance();
    }

//    @Provides
//    @Singleton
//    Realm provideRealm(RealmConfiguration realmConfiguration) {
//        // Get Realm for another thread
//        Realm realm = null;
//        try {
//            realm = Realm.getDefaultInstance();
//        } catch (Exception e) {
//            realm = Realm.getInstance(realmConfiguration);
//        }
//        return realm;
//    }

    @Provides
    @Singleton
    RealmConfiguration provideRealmConfiguration() {
        // Get Realm instance for this thread
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        return config;
    }

    @Provides
    ArticleRealmAdapter provideArticleRealmAdapter(Realm realm) {
        return new ArticleRealmAdapter(realm);
    }

}
