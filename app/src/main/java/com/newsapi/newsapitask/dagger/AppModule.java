package com.newsapi.newsapitask.dagger;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by i7-4770k on 29.01.2018
 */

@Module
public class AppModule {

    private Application application;
    private Context appContext;

    public AppModule(Application application) {
        this.application = application;
        this.appContext = application.getApplicationContext();
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    Context provideAppContext() {
        return appContext;
    }

}
