package com.newsapi.newsapitask.models.response;

import com.google.gson.annotations.SerializedName;
import com.utils.myutils.ConvertDateUtil;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by i7-4770k on 29.01.2018
 */

public class Article extends RealmObject {

    @PrimaryKey
    private int id;
    private boolean favorites;

    @SerializedName("source")
    private Source source;
    @SerializedName("author")
    private String author;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("url")
    private String url;
    @SerializedName("urlToImage")
    private String urlToImage;
    @SerializedName("publishedAt")
    private String publishedAt;

    private String publishedAtFormat;

    public int getPrimaryKey() {
        if (id == 0)
            id = hashCode();
        return id;
    }

    public boolean isFavorites() {
        return favorites;
    }

    public void setFavorites(boolean favorites) {
        this.favorites = favorites;
    }

    public Source getSource() {
        return source;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public String getPublishedAtFormat() {
        if (publishedAtFormat == null)
            publishedAtFormat = ConvertDateUtil.convertDateToString(
                    publishedAt,
                    ConvertDateUtil.YYYY_MM_DD_T_HH_MM_SS_Z,
                    ConvertDateUtil.YYYY_MM_DD_HH_MM);
        return publishedAtFormat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        if (source != null ? !source.equals(article.source) : article.source != null) return false;
        if (author != null ? !author.equals(article.author) : article.author != null) return false;
        if (title != null ? !title.equals(article.title) : article.title != null) return false;
        if (description != null ? !description.equals(article.description) : article.description != null)
            return false;
        if (url != null ? !url.equals(article.url) : article.url != null) return false;
        if (urlToImage != null ? !urlToImage.equals(article.urlToImage) : article.urlToImage != null)
            return false;
        return (publishedAt != null ? !publishedAt.equals(article.publishedAt) : article.publishedAt != null);
    }

    @Override
    public int hashCode() {
        int result = source != null ? source.hashCode() : 0;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (urlToImage != null ? urlToImage.hashCode() : 0);
        result = 31 * result + (publishedAt != null ? publishedAt.hashCode() : 0);
        return result;
    }
}
