package com.newsapi.newsapitask.models;

/**
 * Created by i7-4770k on 29.01.2018
 * Required parameters are missing, the scope of your search is too broad.
 * Please set any of the following required parameters and try again:
 * q, sources, domains.
 */

public class InputDataModel {

    private String where = "top-headlines";
    private String country;
    private String category;
    private String page;
    private String sortBy;
    private String sources;
    private String domains;
    private String from;
    private String to;
    private String keywords;

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public void skipPage() {
        this.page = null;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getSources() {
        return sources;
    }

    public void setSources(String sources) {
        this.sources = sources;
    }

    public String getDomains() {
        return domains;
    }

    public void setDomains(String domains) {
        this.domains = domains;
    }

    public String getFrom() {
        return isEmpteReturnNull(from);
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return isEmpteReturnNull(to);
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getKeywords() {
        return isEmpteReturnNull(keywords);
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public boolean isAllArticles() {
        return where.equals("everything");
    }

    public boolean requestIsPossible() {
        return true;
    }

    private String isEmpteReturnNull(String text) {
        return (text != null && text.isEmpty()) ? null : text;
    }
}
