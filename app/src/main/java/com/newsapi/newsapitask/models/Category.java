package com.newsapi.newsapitask.models;

import com.newsapi.newsapitask.models.response.Source;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by i7-4770k on 29.01.2018
 */

public class Category implements Serializable{

    private String category;

    private List<Source> sources;

    public Category(Source source) {
        if (source != null) {
            this.category = source.getCategory();
            sources = new ArrayList<>();
            addSource(source);
        }
    }

    public void addSource(Source source) {
        if (source != null) {
            sources.add(source);
        }
    }

    public String getCategory() {
        return category;
    }

    public List<Source> getSources() {
        return sources;
    }


}
