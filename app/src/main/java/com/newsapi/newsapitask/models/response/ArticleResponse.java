package com.newsapi.newsapitask.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i7-4770k on 29.01.2018
 */

public class ArticleResponse extends BaseResponse {

    @SerializedName("totalResults")
    private int totalResults;
    @SerializedName("articles")
    private List<Article> articles = new ArrayList<>();

    public int getTotalResults() {
        return totalResults;
    }

    public List<Article> getArticles() {
        return articles;
    }

}
