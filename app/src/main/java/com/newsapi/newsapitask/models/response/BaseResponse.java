package com.newsapi.newsapitask.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by i7-4770k on 29.01.2018
 */

public class BaseResponse {

    @SerializedName("status")
    private String status;
    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;

    public String getStatus() {
        return status;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return status != null && status.equals("ok");
    }

}
