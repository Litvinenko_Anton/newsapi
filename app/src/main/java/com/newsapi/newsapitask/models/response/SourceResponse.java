package com.newsapi.newsapitask.models.response;

import com.google.gson.annotations.SerializedName;
import com.newsapi.newsapitask.models.Category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by i7-4770k on 29.01.2018
 */

public class SourceResponse extends BaseResponse {

    @SerializedName("sources")
    private List<Source> sources = new ArrayList<>();

    private List<Category> categoryList;

    public List<Source> getSources() {
        return sources;
    }

    public List<Category> getCategoryList() {
        if (categoryList == null)
            categoryList = prepareCategoryList(prepareCategoryMap());
        return categoryList;
    }

    private Map<String, Category> prepareCategoryMap() {
        Map<String, Category> categoryMap = new HashMap<>();
        if (sources != null)
            for (Source source : sources) {
                String categoryString = source.getCategory();
                Category category = categoryMap.get(categoryString);
                if (category != null) {
                    category.addSource(source);
                } else {
                    categoryMap.put(categoryString, new Category(source));
                }
            }
        return categoryMap;
    }

    private List<Category> prepareCategoryList(Map<String, Category> categoryMap) {
        List<Category> categoryList = new ArrayList<>();
        for (Map.Entry<String, Category> entry : categoryMap.entrySet()) {
            Category category = entry.getValue();
            categoryList.add(category);
        }
        return categoryList;
    }
}
