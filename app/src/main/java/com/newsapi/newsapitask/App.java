package com.newsapi.newsapitask;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.arellomobile.mvp.MvpFacade;
import com.newsapi.newsapitask.dagger.AppComponent;
import com.newsapi.newsapitask.dagger.AppModule;
import com.newsapi.newsapitask.dagger.DaggerAppComponent;
import com.newsapi.newsapitask.dagger.RealmModule;
import com.newsapi.newsapitask.dagger.RetrofitModule;
import com.utils.myutils.DebugMessageUtils;

/**
 * Created by i7-4770k on 29.01.2018
 */

public class App extends Application {

    public static String BASE_URL;

    @SuppressLint("StaticFieldLeak")
    private static Context context;
    private static AppComponent component;

    public static Context getAppContext() {
        return context;
    }

    public static AppComponent getAppComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initMoxyMvp();
        initAppContext();
        initAppComponent();
        DebugMessageUtils.init(getApplicationContext(), BuildConfig.DEBUG, BuildConfig.DEBUG);
    }

    private void initAppContext() {
        context = getApplicationContext();
    }

    private void initMoxyMvp() {
        MvpFacade.init();
    }

    private void initAppComponent() {

        BASE_URL = "https://newsapi.org/v2/";

        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .retrofitModule(new RetrofitModule(BASE_URL))
                .realmModule(new RealmModule(this))
                .build();
    }

    public static void toast(@Nullable String massage) {
        if (context != null && massage != null)
            Toast.makeText(context, massage, Toast.LENGTH_LONG).show();
    }

}
