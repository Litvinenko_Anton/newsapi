package com.newsapi.newsapitask.tools;

import java.util.List;

import io.realm.RealmObject;

/**
 * Created by i7-4770k on 29.01.2018
 */

public interface RealmStorage<R extends RealmObject> {
    void saveToRealm(R object);

    void saveAllToRealm(List<R> objects);

    void deleteFromRealm(R object);

    void deleteAllFromRealm(List<R> objects);

    boolean containsInRealm(R object);

    boolean containsInRealmFinds(R object);

    R getRealmObjectById(int id);

    void closeConnection();

    void clearRealm();

    void onDestroy();
}
