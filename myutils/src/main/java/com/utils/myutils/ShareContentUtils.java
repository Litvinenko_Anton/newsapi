package com.utils.myutils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

/**
 * Created by Anton on 10.03.2017
 */

public class ShareContentUtils {

    // TODO FaceBook docs
    // https://developers.facebook.com/docs/sharing/android?sdk=maven

    public static void sharedContent(Context context, String subject, String content) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        shareIntent.putExtra(Intent.EXTRA_TEXT, content);
        Intent new_intent = Intent.createChooser(shareIntent, "Share it");
        new_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(new_intent);
    }

    public static void shareToEmail(Context context, String textTitle, String textToSend, String emailToSend, String textError) {
        final Intent emailIntent = new Intent(Intent.ACTION_SENDTO,
                Uri.fromParts("mailto", emailToSend, null));
        emailIntent.setType("*/*");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, textTitle);
        emailIntent.putExtra(Intent.EXTRA_TEXT, textToSend);
        try {
            context.startActivity(Intent.createChooser(emailIntent, "Send email"));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context.getApplicationContext(), textError, Toast.LENGTH_SHORT).show();
        }
    }

}
