package com.utils.myutils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.utils.myutils.ViewUtils.setVisibleOrGoneView;


/**
 * Created by Anton on 19.08.2017 for DEBUG
 * logD -> BLUE   -> DEBUG
 * logI -> GREEN  -> INFO
 * logE -> RED    -> ERROR
 * logV -> WHITE  -> VERBOSE
 * logW -> YELLOW -> WARN
 * logA -> PURPLE -> ASSET
 */

public class DebugMessageUtils {

    @SuppressLint("StaticFieldLeak")
    private static Context context;
    @SuppressLint("StaticFieldLeak")
    private static TextView logTextView;
    private static boolean showLogConfig = true;
    private static boolean showTextConfig = true;

    private static final String TAG = "DEBUG: ";
    private static final String MASSAGE_IS_NULL = "massage == null";
    private static int countLine;
    private static boolean enabledState = true;
    private static SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
    private static Dialog dialog;
    private static float FONT_SIZE = 8f;
    private static int MAX_LINES = 20;
    private static int MAX_LOG_LENGTH = 200;
    @ColorInt
    private static int BACKGROUND = 0xFF2B2B2C;   //BACKGROUND

    @ColorInt
    private static final int WHITE = 0xFFBBBBBB;  //VERBOSE
    @ColorInt
    private static final int BLUE = 0xFF6897BB;   //DEBUG
    @ColorInt
    private static final int GREEN = 0xFF6A8759;  //INFO
    @ColorInt
    private static final int YELLOW = 0xFFBBB529; //WARN
    @ColorInt
    private static final int RED = 0xFFFF6B68;    //ERROR
    @ColorInt
    private static final int PURPLE = 0xFF9876AA; //ASSET

    public static void init(boolean showLog, boolean showText) {
        showLogConfig = showLog;
        showTextConfig = showText;
    }

    public static void init(Context toastContext, boolean showLog, boolean showText) {
        DebugMessageUtils.context = toastContext;
        showLogConfig = showLog;
        showTextConfig = showText;
    }

    public static void toastD(String message) {
        if (showTextConfig && context != null)
            Toast.makeText(context, (message == null) ? MASSAGE_IS_NULL : message, Toast.LENGTH_SHORT).show();
    }

    public static void logD(@Nullable String message) {
        logD(TAG, message); // BLUE
    }

    public static void logD(@NonNull String TAG, @Nullable String massage) {
        if (massage == null) {
            if (showLogConfig)
                Log.d(TAG, MASSAGE_IS_NULL);
            if (showTextConfig)
                logDTV(MASSAGE_IS_NULL);
        } else {
            if (showLogConfig)
                Log.d(TAG, massage);
            if (showTextConfig)
                logDTV(massage);
        }
    }

    public static void logI(@Nullable String message) {
        logI(TAG, message); // GREEN
    }

    public static void logI(@NonNull String TAG, @Nullable String massage) {
        if (massage == null) {
            if (showLogConfig)
                Log.i(TAG, MASSAGE_IS_NULL);
            if (showTextConfig)
                logITV(MASSAGE_IS_NULL);
        } else {
            if (showLogConfig)
                Log.i(TAG, massage);
            if (showTextConfig)
                logITV(massage);
        }
    }

    public static void logE(@Nullable String message) {
        logE(TAG, message); // RED
    }

    public static void logE(@NonNull String TAG, @Nullable String massage) {
        if (massage == null) {
            if (showLogConfig)
                Log.e(TAG, MASSAGE_IS_NULL);
            if (showTextConfig)
                logETV(MASSAGE_IS_NULL);
        } else {
            if (showLogConfig)
                Log.e(TAG, massage);
            if (showTextConfig)
                logETV(massage);
        }
    }


    public static void logV(@Nullable String message) {
        logV(TAG, message); // WHITE
    }

    public static void logV(@NonNull String TAG, @Nullable String massage) {
        if (massage == null) {
            if (showLogConfig)
                Log.v(TAG, MASSAGE_IS_NULL);
            if (showTextConfig)
                logETV(MASSAGE_IS_NULL);
        } else {
            if (showLogConfig)
                Log.v(TAG, massage);
            if (showTextConfig)
                logVTV(massage);
        }
    }


    public static void logW(@Nullable String message) {
        logW(TAG, message); // YELLOW
    }

    public static void logW(@NonNull String TAG, @Nullable String massage) {
        if (massage == null) {
            if (showLogConfig)
                Log.w(TAG, MASSAGE_IS_NULL);
            if (showTextConfig)
                logETV(MASSAGE_IS_NULL);
        } else {
            if (showLogConfig)
                Log.w(TAG, massage);
            if (showTextConfig)
                logWTV(massage);
        }
    }


    public static void logA(@Nullable String message) {
        logA(TAG, message); // PURPLE
    }

    public static void logA(@NonNull String TAG, @Nullable String massage) {
        if (massage == null) {
            if (showLogConfig)
                Log.wtf(TAG, MASSAGE_IS_NULL);
            if (showTextConfig)
                logETV(MASSAGE_IS_NULL);
        } else {
            if (showLogConfig)
                Log.wtf(TAG, massage);
            if (showTextConfig)
                logATV(massage);
        }
    }

    /**
     * LogTextView
     */

    private static void initLogTV() {
        if (showTextConfig) {
            if (logTextView != null) {
                logTextView.setMovementMethod(ScrollingMovementMethod.getInstance());
                logTextView.setTextSize(FONT_SIZE);
                logTextView.setBackgroundColor(BACKGROUND);
                logTextView.setTextColor(WHITE);
                logTextView.setMaxLines(MAX_LINES);
                logTextView.setGravity(Gravity.BOTTOM);
                logTextView.setPadding(10, 0, 10, 0);
                ViewUtils.waitMeasure(logTextView, new ViewUtils.OnMeasuredCallback() {
                    @Override
                    public void onMeasured(View view, int width, int height) {
                        logVTV("TextView isReady");
                    }
                });
            }
        } else {
            ViewUtils.setGoneView(logTextView);
        }
    }

    public static Builder where(@Nullable TextView textView) {
        logTextView = textView;
        if (textView != null)
            dialog = onCreateDialog(textView.getContext());
        return new Builder();
    }

    public static Builder where(@NonNull Activity activity) {
        View viewById = ((ViewGroup) activity.getWindow().getDecorView().findViewById(android.R.id.content)).getChildAt(0);
        TextView textView = new TextView(viewById.getContext());
        if (viewById instanceof RelativeLayout)
            doRelativeLayout((RelativeLayout) viewById, textView);
        else if (viewById instanceof LinearLayout)
            doLinearLayout((LinearLayout) viewById, textView);
        else if (viewById instanceof FrameLayout)
            doFrameLayout((FrameLayout) viewById, textView);
        logTextView = textView;
        dialog = onCreateDialog(textView.getContext());
        return new Builder();
    }

    private static void doRelativeLayout(RelativeLayout relativeLayout, TextView textView) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        textView.setLayoutParams(params);
        relativeLayout.addView(textView);
    }

    private static void doLinearLayout(LinearLayout linearLayout, TextView textView) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.BOTTOM;
        textView.setLayoutParams(params);
        linearLayout.addView(textView);
    }

    private static void doFrameLayout(FrameLayout frameLayout, TextView textView) {
        textView.setLayoutParams(new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.BOTTOM));
        frameLayout.addView(textView);
    }

    public static class Builder {

        private Builder() {
            // private constructor
        }

        public static void setEnabledState(boolean isEnabled) {
            enabledState = isEnabled;
        }

        public Builder setOnOffLongClickableView(@Nullable View onOffLongClickableView) {
            if (showTextConfig && onOffLongClickableView != null)
                onOffLongClickableView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        ViewUtils.setVisibleOrInvisibleView(logTextView, !ViewUtils.isVisibleView(logTextView));
                        return false;
                    }
                });
            return this;
        }

        public Builder setClearLongClickableView(@Nullable View clearLongClickableView) {
            if (showTextConfig && clearLongClickableView != null)
                clearLongClickableView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        showDialog();
                        return false;
                    }
                });
            return this;
        }

        public Builder setVisibility(boolean isVisible) {
            setVisibleOrGoneView(logTextView, isVisible);
            return this;
        }

        public Builder setTextSize(float size) {
            FONT_SIZE = size;
            return this;
        }

        public Builder setMaxLines(int size) {
            MAX_LINES = size;
            return this;
        }

        public Builder setMaxLogLength(int length) {
            MAX_LOG_LENGTH = length;
            return this;
        }

        public Builder setBackgroundColor(@ColorInt int resColor) {
            BACKGROUND = resColor;
            return this;
        }

        public void build() {
            initLogTV();
        }

    }

    public static View getLogTextView() {
        return logTextView;
    }

    public static void clearLogTV() {
        if (showTextConfig) {
            spannableStringBuilder = new SpannableStringBuilder();
            if (logTextView != null)
                logTextView.setText("");
            logVTV("Log Cleared");
        }
    }

    public static void removeLogTV() {
        if (showTextConfig && logTextView != null) {
            logTextView.setOnLongClickListener(null);
            ViewUtils.setGoneView(logTextView);
            logTextView = null;
        }
    }

    public static void logVTV(@Nullable String message) {
        logTV(message, WHITE);
    }

    public static void logDTV(@Nullable String message) {
        logTV(message, BLUE);
    }

    public static void logITV(@Nullable String message) {
        logTV(message, GREEN);
    }

    public static void logWTV(@Nullable String message) {
        logTV(message, YELLOW);
    }

    public static void logETV(@Nullable String message) {
        logTV(message, RED);
    }

    public static void logATV(@Nullable String message) {
        logTV(message, PURPLE);
    }

    public static void logTestTV() {
        logVTV("VERBOSE");
        logDTV("DEBUG");
        logITV("INFO");
        logWTV("WARN");
        logETV("ERROR");
        logATV("ASSET\n");
    }

    public static void logTV(@Nullable String message, @ColorInt int color) {
        if (showTextConfig && enabledState) {
            countLine++;
            String formatMessage;
            if (message == null) {
                formatMessage = countLine + getCurrentTimeToString() + " -> " + "message == NULL" + "\n";
            } else {
                if (message.length() > MAX_LOG_LENGTH) {
                    message = message.substring(0, MAX_LOG_LENGTH);
                    message = message.concat("...");
                }
                formatMessage = countLine + getCurrentTimeToString() + " -> " + message + "\n";
            }
            SpannableString span = new SpannableString(formatMessage);
            span.setSpan(new ForegroundColorSpan(color), 0, formatMessage.length(), 0);
            logTV(span);
        }
    }

    private static void logTV(@Nullable SpannableString spannableString) {
        if (showTextConfig && enabledState) {
            if (spannableString != null)
                spannableStringBuilder.append(spannableString);
            if (logTextView != null)
                printMessage(spannableStringBuilder);
        }
    }

    private static void printMessage(SpannableStringBuilder spannableStringBuilder) {
        logTextView.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);  // print message to text view
    }

    @SuppressLint("SimpleDateFormat")
    private static String getCurrentTimeToString() {
        return new SimpleDateFormat(". HH:mm:ss.SSS").format(new Date(System.currentTimeMillis()));
    }

    private static void showDialog() {
        dialog.show();
    }

    private static Dialog onCreateDialog(Context viewContext) {
        DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Dialog.BUTTON_POSITIVE:
                        clearLogTV();
                        break;
                    case Dialog.BUTTON_NEGATIVE:
                        break;
                    case Dialog.BUTTON_NEUTRAL:
                        removeLogTV();
                        break;
                }
            }
        };

        AlertDialog.Builder adb = new AlertDialog.Builder(viewContext);
        adb.setTitle("LogTextView");
        adb.setMessage("Clare log history?");
        adb.setIcon(android.R.drawable.ic_delete);
        adb.setPositiveButton("Clare", myClickListener);
        adb.setNegativeButton("Cancel", myClickListener);
        adb.setNeutralButton("Remove View", myClickListener);
        return adb.create();                                      // create Dialog
    }

}
