package com.utils.myutils;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Anton on 05.10.2017
 */

public class StringUtils {

    public static boolean isNullOrEmpty(String string) {
        return (string == null || string.isEmpty());
    }

    public static boolean isNullOrEmpty(List list) {
        return (list == null || list.isEmpty());
    }

    public static boolean isNullOrEmpty(Set list) {
        return (list == null || list.isEmpty());
    }

    public static boolean isNullOrEmpty(Map list) {
        return (list == null || list.isEmpty());
    }
//---------------//
    public static boolean noNullNoEmpty(String string) {
        return (string != null && !string.isEmpty());
    }

    public static boolean noNullNoEmpty(List list) {
        return (list != null && !list.isEmpty());
    }

    public static boolean noNullNoEmpty(Set set) {
        return (set != null && !set.isEmpty());
    }

    public static boolean noNullNoEmpty(Map list) {
        return (list != null && !list.isEmpty());
    }

}


