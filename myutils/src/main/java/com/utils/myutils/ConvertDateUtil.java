package com.utils.myutils;

import android.annotation.SuppressLint;
import android.support.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.utils.myutils.StringUtils.noNullNoEmpty;

/**
 * Created by Anton on 28.04.2017
 * /////////// FORMATS /////////////
 * https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
 */

public class ConvertDateUtil {

    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String YYYY_MM_DD_HH_MM = "yyyy-MM-dd hh:mm";
    public static final String YYYY_MM_DD_T_HH_MM_SS_Z = "yyyy-MM-dd'T'HH:mm:ss'Z'";


    public static String convertDateToString(@Nullable String inputDate, String inputDateFormat, String outputDateFormat) {
        if (noNullNoEmpty(inputDate)) {
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dateFormat = new SimpleDateFormat(outputDateFormat);
            return dateFormat.format(convertDateToDate(inputDate, inputDateFormat));
        } else {
            return "";
        }
    }

    public static String convertDateToString(@Nullable Date date, String outputDateFormat) {
        if (date == null)
            date = new Date();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat(outputDateFormat);
        return dateFormat.format(date);
    }

    public static Date convertDateToDate(@Nullable String inputDate, String inputDateFormat) {
        if (noNullNoEmpty(inputDate)) {
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dateFormat = new SimpleDateFormat(inputDateFormat);
            try {
                return dateFormat.parse(inputDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
